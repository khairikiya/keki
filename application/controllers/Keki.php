<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Keki extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Md_users');
    }
    public function index()
    {
        $this->load->view('keki');
    }
    public function prosesLogin()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        if ($this->Md_users->login_user($username, $password)) {
            redirect('spadmin');
        } else {
            $this->session->set_flashdata('error', 'Username & Password salah');
            redirect('keki');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('nama');
        $this->session->unset_userdata('is_login');
        redirect('keki');
    }
}
