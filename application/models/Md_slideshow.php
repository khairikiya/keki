<?php
class Md_slideshow extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function addSlide($data_user)
    {

        $this->db->insert('slideshow', $data_user);
    }

    function getDataSlideshow()
    {
        $sql = "SELECT * FROM slideshow WHERE STATUS = 1";
        $data = $this->db->query($sql);

        return $data->result();
    }
    function updateSlideshow($id, $data)
    {
        $this->db->where('slideshow_id', $id);
        $this->db->update('slideshow', $data);
    }
}
